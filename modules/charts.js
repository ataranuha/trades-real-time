$(function () {
    _app.options = {
        chart: {
            renderTo: 'container',
            zoomType: 'x',
            spacingRight: 20
        },
        title: {
            text: 'Dynamic update via SockJS'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' :
                'Drag your finger over the plot to zoom in'
        },
        xAxis: {
            type: 'datetime',
            maxZoom: 60000,
            title: {
                text: null
            }
        },
        yAxis: {
            title: {
                text: 'Exchange rate'
            },
            showFirstLabel: false
        },
        tooltip: {
            shared: true
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, 'rgba(2,0,0,0)']
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 5
                        }
                    }
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        }
    };
    _app.series = [
        {
            type: 'area',
            name: 'trade',
            pointInterval: 1000,
            pointStart: Date.now(),
            data: [ ]
        }
    ];
    _app.options.series = _app.series;
    console.log(JSON.stringify(_app.series[0].data));
    _app.chart = new Highcharts.Chart(_app.options);
});

