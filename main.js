var _app = {
    connect: function () {

        this.sock = new SockJS($('#hosturl').val(), null, {
            /*protocols_whitelist: [  //working source (phonegap)
                'websocket'
                //'iframe-htmlfile'
                //-'xdr-polling',
                //-'xhr-polling',
                //-'xdr-streaming',
                //-'xhr-streaming',
                //-'iframe-eventsource',
                //'iframe-xhr-polling'//,
                //'jsonp-polling'
            ]*/
        });

        this.sock.onopen = function () {
            var msg = {channels: ["trades_arena"]};
            _app.sock.send(JSON.stringify(msg));
            console.log('open: ' + JSON.stringify(msg));
        };

        this.sock.onmessage = function (e) {
            _app.inMessage(e.data);
        };

        this.sock.onclose = function () {
            setTimeout(function () {
                console.log('reconnect');
                _app.connect();
            }, 500);
            console.log('close');
        };
    },

    inMessage: function (data) {
        var msg = JSON.parse(data);
        console.log(data);
        if (msg.type == 'sysInfo') {
            _app.chart.series[0].addPoint([msg.response.timestamp, msg.response.price]);
            $('#si_price').text(msg.response.price);
            $('#si_listeners').text(msg.response.listeners);
            var divs = $('#log').find('div');
            if (divs.length > 9) {
                divs.first().remove();
            }
            var v = "down";
            if (msg.response.diff > 0) {
                v = "up";
            }
            $('#log').append(
                '<div class="log-row"><i class="icon-arrow-' + v + '"></i><b>'
                    + msg.response.username
                    + '</b> trade: ' + msg.response.price
                    + ' dif: ' + msg.response.diff
                    + '</div>');

        }
    }
};

$('#hosturl').val("http://"+window.location.hostname+':8888/sockjs');

_app.connect();

$('.trade').live('click', function () {
    var cmd = $(this).attr("data-cmd");
    var val = $('#value').val();
    if (cmd == "sell") {
        val = -val;
    }
    var msg = {
        cmd: cmd,
        username: $('#username').val(),
        value: val
    };
    _app.sock.send(JSON.stringify(msg));
    return false;
});



